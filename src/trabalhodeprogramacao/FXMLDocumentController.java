package trabalhodeprogramacao;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import soldados.Guerra;

public class FXMLDocumentController implements Initializable {

    mudaValores muda = new mudaValores();

    //Labels da Quantidade de personagens
    @FXML
    private Label QuanAtiradorTela;
    @FXML
    private Label QuanCavaleiroTela;
    @FXML
    private Label QuanGuerreiroTela;
    @FXML
    private Label QuanBardoTela;
    @FXML
    private Label TotalTropas;

    //Labels Dos recursos em cima da barra
    @FXML
    private Label QuanMadeira;
    @FXML
    private Label QuanComida;
    @FXML
    private Label QuanAgua;
    @FXML
    private Label QuanGold;

    //Text Field dos personagens
    @FXML
    private TextField QuanAtirador;

    @FXML
    private TextField QuanCavaleiro;
    @FXML
    private TextField QuanGuerreiro;
    @FXML
    private TextField QuanBardo;

    //Variaveis globais
    private boolean aprovado;
    private int numPedido = 0, aux = 0;

    @FXML
    private void criaAtirador() {

        numPedido = Integer.parseInt(QuanAtirador.getText());

        aprovado = muda.verificaRecursoAtirador(numPedido);

        if (aprovado) {

            QuanMadeira.setText(Integer.toString(mudaValores.getMadeira()));
            QuanAgua.setText(Integer.toString(mudaValores.getAgua()));
            QuanComida.setText(Integer.toString(mudaValores.getComida()));
            QuanGold.setText(Integer.toString(mudaValores.getGold()));

            aux = numPedido + Guerra.getTodo();

            Guerra.setTodo(aux);

            TotalTropas.setText(Integer.toString(aux));

            aux = numPedido + Guerra.getTodoA();
            Guerra.setTodoA(aux);
            QuanAtiradorTela.setText(Integer.toString(aux));

        } else {
            System.out.println("voce não tem recursos");
        }
    }

    @FXML
    private void criaCavaleiro() {

        numPedido = Integer.parseInt(QuanCavaleiro.getText());
        aprovado = muda.verificaRecursoCavaleiro(numPedido);

        if (aprovado) {
            QuanMadeira.setText(Integer.toString(mudaValores.getMadeira()));
            QuanAgua.setText(Integer.toString(mudaValores.getAgua()));
            QuanComida.setText(Integer.toString(mudaValores.getComida()));
            QuanGold.setText(Integer.toString(mudaValores.getGold()));

            aux = numPedido + Guerra.getTodo();

            Guerra.setTodo(aux);

            TotalTropas.setText(Integer.toString(aux));

            aux = numPedido + Guerra.getTodoC();
            Guerra.setTodoC(aux);
            QuanCavaleiroTela.setText(Integer.toString(aux));

        } else {
            System.out.println("voce não tem recursos");
        }

    }

    @FXML
    private void criaGuerreiro() {
        numPedido = Integer.parseInt(QuanGuerreiro.getText());
        aprovado = muda.verificaRecursoGuerreiro(numPedido);

        if (aprovado) {
            QuanMadeira.setText(Integer.toString(mudaValores.getMadeira()));
            QuanAgua.setText(Integer.toString(mudaValores.getAgua()));
            QuanComida.setText(Integer.toString(mudaValores.getComida()));
            QuanGold.setText(Integer.toString(mudaValores.getGold()));
            aux = numPedido + Guerra.getTodo();

            Guerra.setTodo(aux);

            TotalTropas.setText(Integer.toString(aux));

            aux = numPedido + Guerra.getTodoG();
            Guerra.setTodoG(aux);
            QuanGuerreiroTela.setText(Integer.toString(aux));

        } else {
            System.out.println("voce não tem recursos");
        }
    }

    @FXML
    private void criaBardo() {
        numPedido = Integer.parseInt(QuanBardo.getText());
        aprovado = muda.verificaRecursoBardo(numPedido);

        if (aprovado) {
            QuanMadeira.setText(Integer.toString(mudaValores.getMadeira()));
            QuanAgua.setText(Integer.toString(mudaValores.getAgua()));
            QuanComida.setText(Integer.toString(mudaValores.getComida()));
            QuanGold.setText(Integer.toString(mudaValores.getGold()));

            aux = numPedido + Guerra.getTodo();

            Guerra.setTodo(aux);

            TotalTropas.setText(Integer.toString(aux));

            aux = numPedido + Guerra.getTodoB();
            Guerra.setTodoB(aux);
            QuanBardoTela.setText(Integer.toString(aux));

        } else {
            System.out.println("voce não tem recursos");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualizaTudo();
    }

    @FXML
    public void atualizaTudo() {
        QuanMadeira.setText(Integer.toString(mudaValores.getMadeira()));
        QuanAgua.setText(Integer.toString(mudaValores.getAgua()));
        QuanComida.setText(Integer.toString(mudaValores.getComida()));
        QuanGold.setText(Integer.toString(mudaValores.getGold()));

        if (Guerra.getTodo() > 0) {
            //Continuar os valores aneriores dos soldados 
            QuanAtiradorTela.setText(Integer.toString(Guerra.getTodoA()));
            QuanBardoTela.setText(Integer.toString(Guerra.getTodoB()));
            QuanCavaleiroTela.setText(Integer.toString(Guerra.getTodoC()));
            QuanGuerreiroTela.setText(Integer.toString(Guerra.getTodoG()));
            TotalTropas.setText(Integer.toString(Guerra.getTodo()));
        }
    }

    @FXML
    public void reset() {
        muda.resetDados();
        QuanAtiradorTela.setText(Integer.toString(Guerra.getTodoA()));
        QuanBardoTela.setText(Integer.toString(Guerra.getTodoB()));
        QuanCavaleiroTela.setText(Integer.toString(Guerra.getTodoC()));
        QuanGuerreiroTela.setText(Integer.toString(Guerra.getTodoG()));
        TotalTropas.setText(Integer.toString(Guerra.getTodo()));
        atualizaTudo();
    }

    @FXML

    public void confiGuerreiro() {
        TrabalhoDeProgramacao.trocaTela("GuerreiroConf.fxml");
    }

    @FXML
    public void confiBardo() {
        TrabalhoDeProgramacao.trocaTela("BardoConf.fxml");
    }

    @FXML
    public void confiAtirador() {
        TrabalhoDeProgramacao.trocaTela("AtiradorConf.fxml");
    }

    @FXML
    public void confiCavaleiro() {
        TrabalhoDeProgramacao.trocaTela("CavaleiroConf.fxml");
    }

    @FXML
    public void Irbatalhar() {
        TrabalhoDeProgramacao.trocaTela("Tela2.fxml");

    }
}
