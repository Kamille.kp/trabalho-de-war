package trabalhodeprogramacao;

import java.net.URL;
import java.util.ArrayList;

import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import soldados.Atirador;
import soldados.Bardo;
import soldados.Cavaleiro;
import soldados.Guerra;
import soldados.Guerreiro;

public class GuerraController implements Initializable {

    @FXML
    private Label status;
    @FXML
    private Label sAmigo;
    @FXML
    private Label sInimigo;

    Guerra tropas = new Guerra();

    private int tropaInimigaA = 10, tropaInimigaB = 20, tropaInimigaC = 10, tropaInimigaG = 30;

    ArrayList<Atirador> tropaA = new ArrayList();
    ArrayList<Bardo> tropaB = new ArrayList();
    ArrayList<Cavaleiro> tropaC = new ArrayList();
    ArrayList<Guerreiro> tropaG = new ArrayList();

    ArrayList<Atirador> tropaAI = new ArrayList();
    ArrayList<Bardo> tropaBI = new ArrayList();
    ArrayList<Cavaleiro> tropaCI = new ArrayList();
    ArrayList<Guerreiro> tropaGI = new ArrayList();

    public void criarTropasInimigas() {
        Atirador a1 = new Atirador();
        for (int i = 0; i < tropaInimigaA; i++) {
            tropaAI.add(a1);
        }
        Bardo b1 = new Bardo();
        for (int i = 0; i < tropaInimigaB; i++) {
            tropaBI.add(b1);
        }
        Cavaleiro c1 = new Cavaleiro();
        for (int i = 0; i < tropaInimigaC; i++) {
            tropaCI.add(c1);
        }
        Guerreiro g1 = new Guerreiro();
        for (int i = 0; i < tropaInimigaG; i++) {

            tropaGI.add(g1);
        }

    }

    public void criarTropas() {
        Atirador a = new Atirador();
        a.atribui();
        for (int i = 0; i < Guerra.getTodoA(); i++) {
            tropaA.add(a);
        }
        Bardo b = new Bardo();
        b.atribui();
        for (int i = 0; i < Guerra.getTodoB(); i++) {
            tropaB.add(b);
        }
        Cavaleiro c = new Cavaleiro();
        c.atribui();
        for (int i = 0; i < Guerra.getTodoC(); i++) {

            tropaC.add(c);
        }
        Guerreiro g = new Guerreiro();
        g.atribui();
        for (int i = 0; i < Guerra.getTodoG(); i++) {

            tropaG.add(g);
        }
    }

    private void mostraTropas() {

        sAmigo.setText(Integer.toString(tropaA.size() + tropaB.size() + tropaC.size() + tropaG.size()));
        sInimigo.setText(Integer.toString(tropaAI.size() + tropaBI.size() + tropaCI.size() + tropaGI.size()));
    }

    public void voltar() {
        TrabalhoDeProgramacao.trocaTela("Tela2.fxml");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        criarTropas();
        criarTropasInimigas();
        mostraTropas();
    }

}
