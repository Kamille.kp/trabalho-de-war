
package trabalhodeprogramacao;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import soldados.Guerra;


public class Tela2Controller implements Initializable {
    
    
     @FXML
     Label QuanAtiradortela2;
      @FXML
     Label QuanCavaleirotela2;
      @FXML
     Label QuanGuerreirotela2;
      @FXML
     Label QuanBardotela2;
    
    
    
    
    
     
    @FXML
    public void voltar(){
        TrabalhoDeProgramacao.trocaTela("FXMLDocument.fxml");    
    }
    @FXML
    public void guerrear(){
        TrabalhoDeProgramacao.trocaTela("Guerra.fxml");    
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        QuanAtiradortela2.setText(Integer.toString(Guerra.getTodoA()) );
        QuanBardotela2.setText(Integer.toString(Guerra.getTodoB()));
        QuanCavaleirotela2.setText(Integer.toString(Guerra.getTodoC()));
        QuanGuerreirotela2.setText(Integer.toString(Guerra.getTodoG()));
    }    
        
}
