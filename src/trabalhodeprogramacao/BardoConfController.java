package trabalhodeprogramacao;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import soldados.Bardo;

public class BardoConfController implements Initializable {

    @FXML
    private Label pontos;
    @FXML
    private Label pontoDano;
    @FXML
    private Label pontoVida;
    @FXML
    private Label pontoRes;
    @FXML
    private Label pontoMotivacao;

    private boolean verificado, veri;

    private int top, aux;

    @FXML
    public void aumentaDano() {
        verificado = verificaMax();

        if (verificado) {
            top = Integer.parseInt(pontoDano.getText());
            top++;
            pontoDano.setText(Integer.toString(top));
            Bardo.setSDano(top);
            atualiza(true);
        }

    }

    @FXML
    public void diminuiDano() {
        verificado = verificaMin();
        if (verificado) {
            top = Integer.parseInt(pontoDano.getText());
            top--;
            pontoDano.setText(Integer.toString(top));
            Bardo.setSDano(top);
            atualiza(false);
        }

    }

    @FXML
    public void aumentaVida() {
        verificado = verificaMax();

        if (verificado) {
            top = Integer.parseInt(pontoVida.getText());
            top++;
            pontoVida.setText(Integer.toString(top));
            Bardo.setSVida(top);
            atualiza(true);
        }

    }

    @FXML
    public void diminuiVida() {
        verificado = verificaMin();
        if (verificado) {
            top = Integer.parseInt(pontoVida.getText());
            top--;
            pontoVida.setText(Integer.toString(top));
            Bardo.setSVida(top);
            atualiza(false);
        }

    }

    @FXML
    public void aumentaRes() {
        verificado = verificaMax();

        if (verificado) {
            top = Integer.parseInt(pontoRes.getText());
            top++;
            pontoRes.setText(Integer.toString(top));
            Bardo.setSResistencia(top);
            atualiza(true);
        }

    }

    @FXML
    public void diminuiRes() {
        verificado = verificaMin();
        if (verificado) {
            top = Integer.parseInt(pontoRes.getText());
            top--;
            pontoRes.setText(Integer.toString(top));
            Bardo.setSResistencia(top);
            atualiza(false);
        }

    }

    @FXML
    public void aumentaMotivacao() {
        verificado = verificaMax();

        if (verificado) {
            top = Integer.parseInt(pontoMotivacao.getText());
            top++;
            pontoMotivacao.setText(Integer.toString(top));
            Bardo.setSMotivacao(top);
            atualiza(true);
        }

    }

    @FXML
    public void diminuiMotivacao() {
        verificado = verificaMin();
        if (verificado) {
            top = Integer.parseInt(pontoMotivacao.getText());
            top--;
            pontoMotivacao.setText(Integer.toString(top));
            Bardo.setSMotivacao(top);
            atualiza(false);
        }

    }

    @FXML
    public void atualiza(boolean vai) {
        aux = Integer.parseInt(pontos.getText());

        if (vai) {
            aux--;
        } else {
            aux++;
        }
        Bardo.setNumeroChance(aux);
        pontos.setText(Integer.toString(aux));
    }

    @FXML
    private boolean verificaMax() {
        veri = false;
        aux = Integer.parseInt(pontos.getText());
        if (aux > 0) {
            veri = true;
        }

        return veri;
    }

    private boolean verificaMin() {
        veri = false;
        aux = Integer.parseInt(pontos.getText());
        if (aux >= 0 && aux < 10) {
            veri = true;
        }

        return veri;
    }

    @FXML
    public void voltar() {
        TrabalhoDeProgramacao.trocaTela("FXMLDocument.fxml");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
         pontos.setText(Integer.toString(Bardo.getNumeroChance()));
        pontoDano.setText(Integer.toString(Bardo.getSDano()));
        pontoMotivacao.setText(Integer.toString(Bardo.getSMotivacao()));
        pontoRes.setText(Integer.toString(Bardo.getSResistencia()));
        pontoVida.setText(Integer.toString(Bardo.getSVida()));

    }

}
