/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soldados;

public class Guerreiro extends Exercito {

    private static int SDano = 0, SVida = 0, SResistencia = 0, SMotivacao = 0, numeroChance = 10;

    public void Guerreiro() {
        setDano(80);
        setVida(120);
        setResistencia(80);
    }
public void atribui(){
        setDano(80+SDano);
       setVida(120+SVida);
       setResistencia(80+SResistencia);
       setMotivacao(SMotivacao);
    }
    public static int getSDano() {
        return SDano;
    }

    public static void setSDano(int SDano) {
        Guerreiro.SDano = SDano;
    }

    public static int getSVida() {
        return SVida;
    }

    public static void setSVida(int SVida) {
        Guerreiro.SVida = SVida;
    }

    public static int getSResistencia() {
        return SResistencia;
    }

    public static void setSResistencia(int SResistencia) {
        Guerreiro.SResistencia = SResistencia;
    }

    public static int getSMotivacao() {
        return SMotivacao;
    }

    public static void setSMotivacao(int SMotivacao) {
        Guerreiro.SMotivacao = SMotivacao;
    }

    public static int getNumeroChance() {
        return numeroChance;
    }

    public static void setNumeroChance(int numeroChance) {
        Guerreiro.numeroChance = numeroChance;
    }

}
